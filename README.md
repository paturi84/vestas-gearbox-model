#Vestas Gearbox Model
This README will be acting as a guide to run and execute the ge-generator model. The code is not modularized and not object-oriented yet. 
If required it can be made into obj oriented with minimal effort. 
Additional details about the model can be found here in [0036 - Vestas Gearbox Model](https://confluence.nexteraenergy.com/display/PM/0036+-+Vestas+Gearbox+Model+-+Under+construction)

##Code Components
There are three .py files

1.	vestas-gearbox-create-target-file.py - This file can be used to create targets from the Master table
2.	vestas-train-model.py - This file contains code to train the model
3.	vestas-create-predictions.py - This file contains code to create predictions

###1. Create Targets

This file will create targets from Master table. The master table is stored in this location: [Master_Table_s3_location](s3://pdm-wbi/sam/Master_Table/Master Table.xlsx). This in turn is sourced from: [Shared_Drive](\\jbxsw65\pgen\FPLEnergy\PowerGen\Wind\Production Assurance\Master Table\Current Master Table). The shared drive location is updated by Hussain, Taahyr on a monthly basis (usually by 4th or 5th of every month). This s3 file is read and the code will find targets based on pre-defined criteria and come up with a list. This is useful when re-training the model. The s3 location contains the updated Master Table file for the month of June 2019. If it needs to be uploaded to the s3 location use the following command: `aws s3 cp '//jbxsw65/pgen/FPLEnergy/PowerGen/Wind/Production Assurance/Master Table/Current Master Table/Master Table.xlsx' 's3://pdm-wbi/sam/Master_Table/Master Table.xlsx'`

###2. Train Model

The pre-processed dataset is stored in the following location: [Training_DataSet](s3://pdm-wbi/sam/Vestas_Gearbox/training_data/training_data.csv). The code will train the model on using the pre-defined conditions and store the model on the [s3 location](s3://pdm-wbi/sam/Vestas_Gearbox/models/vestas_model.pickle) as a pickle file. Given that this model has been retrained in Jun it may not need to be retrained till Sep/Oct 2019.

** Note: When training the model, the line 118 
```vestas_train = vestas_pdf_trunc_with_target[vestas_pdf_trunc_with_target['Start_Date'] <= '2019-03-31']``` **
** Basically, you are specifying which data to train on. The recommendation is, if running for Jun, train till May data. So, the new date would become '2019-05-31'. Make sure to have a full month's data to predict. **
###3. Create Predictions

This file has the code that is needed to prepare new data and generate predictions. As of now the predictions are being stored as csv file, but it can be configured later to be sent to predictions db directly. 

*Note: This script Does NOT need wbi data processor to be run. It will go through all the vestas folders and create the data needed based on the date selected and train model.*

###Steps to run the .py files:

1. 

    * Change the keypair and related tags `(Createdby and CreatedOn=mm/dd/yyyy)`
    * Create the cluster using the script: ```aws emr create-cluster --applications Name=Spark Name=Zeppelin --tags 'AgileTeam=PdM' 'Environment=PdM' 'CreatedBy=Samson Paturi' 'CreatedOn=06/10/2019 19:04 EDT' --ec2-attributes '{"KeyName":"SP_KeyPair","AdditionalSlaveSecurityGroups":["sg-16b8cf64","sg-4d21e638"],"InstanceProfile":"GDMS-EMR-EC2","SubnetId":"subnet-ecde8888","EmrManagedSlaveSecurityGroup":"sg-c4a04cb1","EmrManagedMasterSecurityGroup":"sg-8fa24efa","AdditionalMasterSecurityGroups":["sg-16b8cf64","sg-4d21e638"]}' --release-label emr-5.23.0 --log-uri 's3n://aws-logs-422176158307-us-east-1/elasticmapreduce/' --instance-groups '[{"InstanceCount":1,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":160,"VolumeType":"gp2"},"VolumesPerInstance":1}]},"InstanceGroupType":"MASTER","InstanceType":"m5.12xlarge","Name":"Master - 1"},{"InstanceCount":1,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":160,"VolumeType":"gp2"},"VolumesPerInstance":1}]},"InstanceGroupType":"CORE","InstanceType":"m5.4xlarge","Name":"Core - 2"}]' --auto-scaling-role EMR_AutoScaling_DefaultRole --ebs-root-volume-size 10 --service-role GDMS-EMR --security-configuration 'gdms-security-configuration' --enable-debugging --name 'vestas-gearbox' --scale-down-behavior TERMINATE_AT_TASK_COMPLETION --region us-east-1```
    
2. 

    * ssh into the cluster and install the required packages listed under the `requirements.txt` file `ssh -i [keypair] -L 8895:localhost:8888 hadoop@[cluster's private IP address]` and `sudo yum update -y`
    * Create WinSCP session to place files in EMR cluster, then run the commands: `sudo pip install -r requirements.txt `and `sudo python3 -m pip install -r requirements.txt`
    * To make python3 as the default python: `sudo sed -i -e '$a\export PYSPARK_PYTHON=/usr/bin/python3' /etc/spark/conf/spark-env.sh`
    
3. Now it is possible to launch a Jupyter notebook and run the code step by step
4.  

    * If you are going to be using spark you need to set the environment: 
    
        * `export HADOOP_HOME=/usr/lib/hadoop/` 
        * `export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop`
        * `export YARN_CONF_DIR=$HADOOP_HOME/etc/hadoop`
        * `export SPARK_HOME=/usr/lib/spark/`
        
    * Then execute spark-submit job: 
    
        ```
        spark-submit --master yarn --driver-memory 160g --packages org.apache.spark:spark-avro_2.11:2.4.0,com.databricks:spark-csv_2.11:1.5.0 vestas-train-model.py
        ```

5. Note: You can use the same cluster to run all the py files. **The code contains Spark DF to Pandas DF conversion so the master node needs to be a large instance \(m5.12x\) to ensure that that step completes successfully.** Training is expected to take about 90 minutes with the cluster as configured

##Model Performance: 

As outlined in the model description page [0036 - Vestas Gearbox Model](https://confluence.nexteraenergy.com/display/PM/0036+-+Vestas+Gearbox+Model+-+Under+construction) this model has a 32% precision and 46% recall at an observational level. In reality it may perform anywhere between 15-25% precision. 
