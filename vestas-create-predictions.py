import pandas as pd
import numpy as np
import pickle
import os
# os.environ['PYSPARK_SUBMIT_ARGS'] = '--master yarn --driver-memory 160g \
#                                     --packages org.apache.spark:spark-avro_2.11:2.4.0,com.databricks:spark-csv_2.11:1.5.0 pyspark-shell'
from xgboost import XGBClassifier
from sklearn.preprocessing import MinMaxScaler, LabelEncoder
import findspark
findspark.init()
from pyspark.shell import spark
from pyspark.sql import DataFrame
from pyspark.sql.functions import *
from functools import reduce  # For Python 3.x
from pyspark.sql.functions import isnan, col, countDistinct, from_unixtime, window, lit, count, when, to_timestamp, unix_timestamp, to_date
import io
import ast
import re
import s3fs
pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_columns', None)
spark.conf.set("spark.sql.execution.arrow.enabled", "true")
import datetime

# Create prediction dataset

v47 = spark.read.format("avro").load("s3://gdms-windbi-mirror/turbine-technology-fact/monthly/dw.vestasv47turbinefact/*/*/*/*.avro")
v80 = spark.read.format("avro").load("s3://gdms-windbi-mirror/turbine-technology-fact/monthly/dw.vestasv80turbinefact/*/*/*/*.avro")

v47_cols = v47.columns
v80_cols = v80.columns
common_cols = list(set(v47_cols).intersection(set(v80_cols)))
common_cols.sort() #Sort it in alphabetical order
cols_to_drop = ['AMBIENTTEMPOKSECONDS', 'CNTRENERGYNET','CNTRONLINEHOURS','CNTRREACTIVEENERGYNET','COMMUNICATIONERROR','ENERGYCONSUMPTION','ENERGYNET','ENERGYPRODUCTION','FUNCTIONINTERVAL','GRIDOKSECONDS','LOADUTCDATETIME','LOCALDATETIME','MAXCURRENTPHASEA','MAXCURRENTPHASEB','MAXCURRENTPHASEC','MAXFREQUENCY','MAXHYDRAULICTEMPERATURE','MEANCTMBATTERYVOLTAGE','MEANCURRENTPHASEA','MEANCURRENTPHASEB','MEANCURRENTPHASEC','MEANHYDRAULICTEMPERATURE','MEANPHASE1VOLTAGE','MEANPHASE2VOLTAGE','MEANPHASE3VOLTAGE','MEANTOPBOXTEMPERATURE','MINBLADEPITCHANGLE','MINCTMBATTERYVOLTAGE','MINFREQUENCY','ONLINESECONDS','OPERATINGSECONDS','REACTIVEENERGYCONSUMPTION','REACTIVEENERGYNET','REACTIVEENERGYPRODUCTION','SERVICESECONDS','SERVICESTATE','STDDEVBLADEPITCHANGLE','SWISTATUS','TIMEOFMAXFREQUENCY','TIMEOFMAXGENERATORSPEED','TIMEOFMAXPOWER','TIMEOFMAXWINDSPEED','TIMEOFMINFREQUENCY','TOTALSECONDS','TURBINEOKSECONDS','WINDSPEEDOKSECONDS','YAWLEFTCYCLES','YAWLEFTONTIME','YAWRIGHTCYCLES','YAWRIGHTONTIME','YAWSECONDS','YAWSTATE']
cols_needed = [col for col in common_cols if col not in cols_to_drop]
#len(cols_needed) = 46
v47 = v47[cols_needed]
v80 = v80[cols_needed]

def unionAll(*dfs):
    return reduce(DataFrame.union, dfs)

vestas = unionAll(v47, v80)

vestas_cols_that_dont_need_aggregation = ['DATASTATUSSK','DATESK','FAULTCODE','FAULTCODENAME','OPERATIONCODE','PLANTSK''TURBINESK','TURBINESTATECODE','TURBINESTATUS','UTCDATETIME']
vestas_cols_to_aggregate = [col for col in vestas.columns if col not in vestas_cols_that_dont_need_aggregation]

# Create a dictionary which contains the type of aggregation based on a column list
exprs1 = {x: "min" for x in vestas_cols_to_aggregate if x.startswith('MIN')}
exprs2 = {x: "max" for x in vestas_cols_to_aggregate if x.startswith('MAX')}
exprs3 = {x: "max" for x in vestas_cols_to_aggregate if x.startswith('OPERATINGSTATEANDFAULT')}
exprs4 = {x: "mean" for x in vestas_cols_to_aggregate if x.startswith('MEAN')}
exprs5 = {x: "mean" for x in vestas_cols_to_aggregate if x.startswith('STDDEV')}
exprs6 = {x: "mean" for x in vestas_cols_to_aggregate if x.startswith('CMP')}
exprs7 = {x: "mean" for x in vestas_cols_to_aggregate if not x.startswith(('MIN', 'MAX', 'MEAN', 'STDDEV'))}

# Combine all aggregations
exprs = dict(list(exprs1.items()) + list(exprs2.items()) + list(exprs3.items()) + list(exprs4.items()) + list(exprs5.items()) + list(exprs6.items()) + list(exprs7.items()))

#oracle_time_format = "MM/dd/yyyy hh:mm:ss aa"
aggregation_interval = "60 minutes"
vestas = vestas.withColumn("UTCDATETIME", from_unixtime(col("UTCDATETIME")/1000).cast("timestamp"))

#Aggregate all the continuous columns by the expressions like min, max, stdev, avg etc
vestas_agg_by_turbine = vestas.groupBy("TURBINESK", window("UTCDATETIME", aggregation_interval)).agg(exprs)
#vestas_agg_by_turbine.columns
vestas_agg_by_turbine = vestas_agg_by_turbine.drop('avg(TURBINESK)') #It was observed that the TURBINESK column was not
                                                                     #dropped when v47 and v80 were joined so it got
                                                                     #aggregated by avg, so dropping it
vestas_aggregated = vestas_agg_by_turbine.toDF(*(re.sub("^min", "", re.sub("^max", "", re.sub("^avg","",re.sub("\(|\)","",col)))) for col in vestas_agg_by_turbine.columns))
#vestas_aggregated.columns just to check if the duplicate TURBINESK column got removed
vestas_aggregated = vestas_aggregated.sort("TURBINESK", "window") #Sort by turbine and timestamp

vestas_aggregated = vestas_aggregated.withColumn("start", to_timestamp("window.start"))\
                                     .withColumn("end", to_timestamp("window.end"))\
                                     .drop('window')

vestas_aggregated= vestas_aggregated.withColumn('Start_Date', to_date("start", "yyyyMMdd"))\
                                     .withColumn('End_Date', to_date("end", "yyyyMMdd"))

#This line needs to be updated to change the dates for which predictions are needed. 
vestas_month_to_predict = vestas_aggregated[(vestas_aggregated['Start_Date'] > '2019-04-01') & (vestas_aggregated['Start_Date'] < '2019-06-01')]

cols_to_train = ['TURBINESK', 'Start_Date', 'MEANGENERATORSPEED', 'MINAMBIENTAIRTEMPERATURE', 'MINWINDSPEED', 'MEANGEARBOXSUMPTEMPERATURE',\
                 'MEANHUBSPEED', 'STDDEVPOWER',  'MAXPOWER',  'MINGEARBOXSUMPTEMPERATURE', 'MEANGENERATOR1TEMPERATURE',  'MAXHUBCONTROLLERTEMPERATURE',\
                 'MAXGENERATORSPEED', 'MEANPOWER', 'MINPOWER', 'MEANGEARTEMPERATURE', 'MAXWINDSPEED', 'RELATIVEWINDDIRECTION', 'MEANWINDSPEED',\
                 'STDDEVGENERATORSPEED', 'MAXAMBIENTAIRTEMPERATURE', 'STDDEVHUBSPEED', 'MAXBLADEPITCHANGLE', 'ABSOLUTENACELLEPOSITION', \
                 'ABSOLUTEWINDDIRECTION', 'MEANAMBIENTAIRTEMPERATURE', 'MINHUBSPEED', 'MINBEARINGTEMPERATURE', 'MAXBEARINGTEMPERATURE', \
                 'MEANNACELLETEMPERATURE', 'MINGENERATORSPEED', 'DUPLICATEWINDSPEED', 'STDDEVWINDSPEED', 'MEANBEARINGTEMPERATURE', 'MEANFREQUENCY',\
                  'MAXGEARBOXSUMPTEMPERATURE', 'MEANPOWERFACTOR', 'MAXHUBSPEED']

vestas_month_to_predict_pdf = vestas_month_to_predict.toPandas()

fs = s3fs.S3FileSystem()
xgb_model = pickle.load(fs.open('s3://pdm-wbi/sam/Vestas_Gearbox/models/vestas_model.pickle', 'rb'))
scaler = MinMaxScaler(feature_range=(-1,1))
X_pred = vestas_month_to_predict_pdf[cols_to_train].drop(['TURBINESK', 'Start_Date'], axis=1)

predictions = xgb_model.predict(X_pred.values)
pred_prob = xgb_model.predict_proba(X_pred.values).astype('float32')[:,1]
vestas_month_to_predict_pdf['Predicted_Failure'] = predictions
vestas_month_to_predict_pdf['Failure_Probability'] = pred_prob
pivoted = pd.pivot_table((vestas_month_to_predict_pdf[['TURBINESK', 'Predicted_Failure', 'Failure_Probability']]), index=['TURBINESK', 'Predicted_Failure'], aggfunc=[np.mean])
flattened = pd.DataFrame(pivoted.to_records())
flattened.columns = ['TURBINESK', 'Predicted_Failure', 'Failure_Probability']
pivoted1 = pd.pivot_table((vestas_month_to_predict_pdf[['TURBINESK', 'Predicted_Failure', 'Failure_Probability']]), index=['TURBINESK', 'Predicted_Failure'], aggfunc=[np.count_nonzero])
flattened1 = pd.DataFrame(pivoted1.to_records())
flattened1.columns = ['TURBINESK', 'Predicted_Failure', 'Failure_Counts']
flattened['Failure_Counts'] = flattened1['Failure_Counts']
del flattened1
flattened1 = flattened[flattened['Predicted_Failure'] == 0]
flattened1.columns = ['TURBINESK', 'Predicted_Zero', 'Probability_Zero', 'Count_Zero']
flattened = flattened[flattened['Predicted_Failure'] == 1]
flattened.columns = ['TURBINESK', 'Predicted_One', 'Probability_One', 'Count_One']
flattened = flattened.merge(flattened1,how='left', on='TURBINESK')
flattened['Percent_One'] = (flattened['Count_One']/(flattened['Count_One'] + flattened['Count_Zero'])) * 100
flattened['Weighted_Score'] = flattened['Percent_One'] * flattened['Probability_One']
flattened = flattened.sort_values(by=['Weighted_Score'])  # Creating a weighted score by multiplying Probability_One and percent of one
# We would like to create predictions when the Weighted_Score is above a threshold (30%)
# so the below code populates the column the 'Create_Prediction' as 'Yes' for the turbines above threshold
# threshold = 30.0 # this is the minimum weighted score above which a prediction should be created
flattened['Prediction_Needed'] = np.where((flattened['Weighted_Score'] >= 30), 'Yes', "No")
result_df = flattened[flattened['Prediction_Needed'] == 'Yes']
result_df.to_csv('s3://pdm-wbi/sam/Vestas_Gearbox/predictions/apr_may_2019.csv')
print(result_df)