import pandas as pd
import numpy as np
import pickle
import os
# os.environ['PYSPARK_SUBMIT_ARGS'] = '--master yarn --driver-memory 160g \
#                                      --packages org.apache.spark:spark-avro_2.11:2.4.0,com.databricks:spark-csv_2.11:1.5.0 pyspark-shell'
from xgboost import XGBClassifier
from sklearn.metrics import classification_report, precision_score, precision_recall_curve
from sklearn.model_selection import train_test_split, cross_val_score, cross_validate, cross_val_predict
from sklearn.preprocessing import MinMaxScaler, LabelEncoder
import findspark
findspark.init()
from pyspark.shell import spark
from pyspark.sql import DataFrame
from pyspark.sql.functions import *
from functools import reduce  # For Python 3.x
from pyspark.sql.functions import isnan, col, countDistinct, from_unixtime, window, lit, count, when, to_timestamp, unix_timestamp, to_date
import io
import ast
import re
import s3fs
pd.set_option('display.max_colwidth', -1)
pd.set_option('display.max_columns', None)
spark.conf.set("spark.sql.execution.arrow.enabled", "true")
import datetime

# Create prediction dataset

v47 = spark.read.format("avro").load("s3://gdms-windbi-mirror/turbine-technology-fact/monthly/dw.vestasv47turbinefact/*/*/*/*.avro")
v80 = spark.read.format("avro").load("s3://gdms-windbi-mirror/turbine-technology-fact/monthly/dw.vestasv80turbinefact/*/*/*/*.avro")

v47_cols = v47.columns
v80_cols = v80.columns
common_cols = list(set(v47_cols).intersection(set(v80_cols)))
common_cols.sort() #Sort it in alphabetical order
cols_to_drop = ['AMBIENTTEMPOKSECONDS', 'CNTRENERGYNET','CNTRONLINEHOURS','CNTRREACTIVEENERGYNET','COMMUNICATIONERROR','ENERGYCONSUMPTION','ENERGYNET','ENERGYPRODUCTION','FUNCTIONINTERVAL','GRIDOKSECONDS','LOADUTCDATETIME','LOCALDATETIME','MAXCURRENTPHASEA','MAXCURRENTPHASEB','MAXCURRENTPHASEC','MAXFREQUENCY','MAXHYDRAULICTEMPERATURE','MEANCTMBATTERYVOLTAGE','MEANCURRENTPHASEA','MEANCURRENTPHASEB','MEANCURRENTPHASEC','MEANHYDRAULICTEMPERATURE','MEANPHASE1VOLTAGE','MEANPHASE2VOLTAGE','MEANPHASE3VOLTAGE','MEANTOPBOXTEMPERATURE','MINBLADEPITCHANGLE','MINCTMBATTERYVOLTAGE','MINFREQUENCY','ONLINESECONDS','OPERATINGSECONDS','REACTIVEENERGYCONSUMPTION','REACTIVEENERGYNET','REACTIVEENERGYPRODUCTION','SERVICESECONDS','SERVICESTATE','STDDEVBLADEPITCHANGLE','SWISTATUS','TIMEOFMAXFREQUENCY','TIMEOFMAXGENERATORSPEED','TIMEOFMAXPOWER','TIMEOFMAXWINDSPEED','TIMEOFMINFREQUENCY','TOTALSECONDS','TURBINEOKSECONDS','WINDSPEEDOKSECONDS','YAWLEFTCYCLES','YAWLEFTONTIME','YAWRIGHTCYCLES','YAWRIGHTONTIME','YAWSECONDS','YAWSTATE']
cols_needed = [col for col in common_cols if col not in cols_to_drop]
#len(cols_needed) = 46
v47 = v47[cols_needed]
v80 = v80[cols_needed]

def unionAll(*dfs):
    return reduce(DataFrame.union, dfs)

vestas = unionAll(v47, v80)

vestas_cols_that_dont_need_aggregation = ['DATASTATUSSK','DATESK','FAULTCODE','FAULTCODENAME','OPERATIONCODE','PLANTSK''TURBINESK','TURBINESTATECODE','TURBINESTATUS','UTCDATETIME']
vestas_cols_to_aggregate = [col for col in vestas.columns if col not in vestas_cols_that_dont_need_aggregation]

# Create a dictionary which contains the type of aggregation based on a column list
exprs1 = {x: "min" for x in vestas_cols_to_aggregate if x.startswith('MIN')}
exprs2 = {x: "max" for x in vestas_cols_to_aggregate if x.startswith('MAX')}
exprs3 = {x: "max" for x in vestas_cols_to_aggregate if x.startswith('OPERATINGSTATEANDFAULT')}
exprs4 = {x: "mean" for x in vestas_cols_to_aggregate if x.startswith('MEAN')}
exprs5 = {x: "mean" for x in vestas_cols_to_aggregate if x.startswith('STDDEV')}
exprs6 = {x: "mean" for x in vestas_cols_to_aggregate if x.startswith('CMP')}
exprs7 = {x: "mean" for x in vestas_cols_to_aggregate if not x.startswith(('MIN', 'MAX', 'MEAN', 'STDDEV'))}

# Combine all aggregations
exprs = dict(list(exprs1.items()) + list(exprs2.items()) + list(exprs3.items()) + list(exprs4.items()) + list(exprs5.items()) + list(exprs6.items()) + list(exprs7.items()))

#oracle_time_format = "MM/dd/yyyy hh:mm:ss aa"
aggregation_interval = "60 minutes"
from pyspark.sql.functions import isnan, col, countDistinct, from_unixtime, window, lit, count, when, to_timestamp, unix_timestamp, to_date
vestas = vestas.withColumn("UTCDATETIME", from_unixtime(col("UTCDATETIME")/1000).cast("timestamp"))
#vestas.select('UTCDATETIME').show(4)
#Aggregate all the continuous columns by the expressions like min, max, stdev, avg etc
vestas_agg_by_turbine = vestas.groupBy("TURBINESK", window("UTCDATETIME", aggregation_interval)).agg(exprs)
#vestas_agg_by_turbine.columns
vestas_agg_by_turbine = vestas_agg_by_turbine.drop('avg(TURBINESK)') #It was observed that the TURBINESK column was not
                                                                     #dropped when v47 and v80 were joined so it got
                                                                     #aggregated by avg, so dropping it
vestas_aggregated = vestas_agg_by_turbine.toDF(*(re.sub("^min", "", re.sub("^max", "", re.sub("^avg","",re.sub("\(|\)","",col)))) for col in vestas_agg_by_turbine.columns))
#vestas_aggregated.columns just to check if the duplicate TURBINESK column got removed
vestas_aggregated = vestas_aggregated.sort("TURBINESK", "window") #Sort by turbine and timestamp

vestas_aggregated = vestas_aggregated.withColumn("start", to_timestamp("window.start"))\
                                     .withColumn("end", to_timestamp("window.end"))\
                                     .drop('window')

vestas_aggregated= vestas_aggregated.withColumn('Start_Date', to_date("start", "yyyyMMdd"))\
                                     .withColumn('End_Date', to_date("end", "yyyyMMdd"))

#Read the targets file
vestas_targets = spark.read.csv("s3://pdm-wbi/sam/Vestas_Gearbox/vestas_target_file.csv", header=True)
vestas_targets = vestas_targets.withColumn('TURBINESK', col('TURBINESK').cast('long')) \
                               .withColumn('Failure_Start_Date', to_date('Failure_Start_Date', 'MM/dd/yyyy')) \
                               .withColumn('Failure_End_Date', to_date('Failure_End_Date', 'MM/dd/yyyy'))

all_turbs = set(vestas_aggregated.select("TURBINESK").rdd.flatMap(lambda x: x).distinct().collect())
len(all_turbs)
target_turbs = set(vestas_targets.select("TURBINESK").rdd.flatMap(lambda x: x).distinct().collect())
len(target_turbs)
common_turbs = set(all_turbs).intersection(target_turbs)
len(common_turbs)
vestas_uniq_turbs = list(all_turbs.difference(common_turbs))
len(vestas_uniq_turbs)
vestas_uniq_turbs_trunc = list(vestas_uniq_turbs[:int(0.2*len(vestas_uniq_turbs))])
len(vestas_uniq_turbs_trunc)
turbs_to_keep = vestas_uniq_turbs_trunc + list(common_turbs)
len(turbs_to_keep)
vestas_aggregated_trunc = vestas_aggregated[vestas_aggregated['TURBINESK'].isin(turbs_to_keep)]

vestas_pdf_trunc = vestas_aggregated_trunc.toPandas()

vestas_targets_pdf = pd.read_csv('s3://pdm-wbi/sam/Vestas_Gearbox/vestas_target_file.csv')
vestas_targets_pdf = vestas_targets_pdf.drop(['Location Plant'], axis=1)
vestas_targets_pdf['Failure_Start_Date'] = pd.to_datetime(vestas_targets_pdf['Failure_Start_Date'])
vestas_targets_pdf['Failure_End_Date'] = pd.to_datetime(vestas_targets_pdf['Failure_End_Date'])

vestas_pdf_trunc_with_target = vestas_pdf_trunc.merge(vestas_targets_pdf, on=['TURBINESK'], how='left')
vestas_pdf_trunc_with_target['Failure'] = 0
vestas_pdf_trunc_with_target['Start_Date'] = pd.to_datetime(vestas_pdf_trunc_with_target['Start_Date'])
vestas_pdf_trunc_with_target.loc[(vestas_pdf_trunc_with_target['Start_Date'] >= vestas_pdf_trunc_with_target['Failure_Start_Date']) & \
       (vestas_pdf_trunc_with_target['Start_Date'] <= vestas_pdf_trunc_with_target['Failure_End_Date']), 'Failure'] = 1

vestas_train = vestas_pdf_trunc_with_target[vestas_pdf_trunc_with_target['Start_Date'] <= '2019-03-31'] #Leave out the months you want to predict
# vestas_predict = vestas_pdf_trunc_with_target[vestas_pdf_trunc_with_target['Start_Date'] > '2019-03-31']
cols_to_train = ['TURBINESK', 'Start_Date', 'MEANGENERATORSPEED', 'MINAMBIENTAIRTEMPERATURE', 'MINWINDSPEED', 'MEANGEARBOXSUMPTEMPERATURE',\
                 'MEANHUBSPEED', 'STDDEVPOWER',  'MAXPOWER',  'MINGEARBOXSUMPTEMPERATURE', 'MEANGENERATOR1TEMPERATURE',  'MAXHUBCONTROLLERTEMPERATURE',\
                 'MAXGENERATORSPEED', 'MEANPOWER', 'MINPOWER', 'MEANGEARTEMPERATURE', 'MAXWINDSPEED', 'RELATIVEWINDDIRECTION', 'MEANWINDSPEED',\
                 'STDDEVGENERATORSPEED', 'MAXAMBIENTAIRTEMPERATURE', 'STDDEVHUBSPEED', 'MAXBLADEPITCHANGLE', 'ABSOLUTENACELLEPOSITION', \
                 'ABSOLUTEWINDDIRECTION', 'MEANAMBIENTAIRTEMPERATURE', 'MINHUBSPEED', 'MINBEARINGTEMPERATURE', 'MAXBEARINGTEMPERATURE', \
                 'MEANNACELLETEMPERATURE', 'MINGENERATORSPEED', 'DUPLICATEWINDSPEED', 'STDDEVWINDSPEED', 'MEANBEARINGTEMPERATURE', 'MEANFREQUENCY',\
                  'MAXGEARBOXSUMPTEMPERATURE', 'MEANPOWERFACTOR', 'MAXHUBSPEED']

# Begin train
X = vestas_train[cols_to_train].drop(['TURBINESK', 'Start_Date'], axis=1)
y = vestas_train['Failure']
scaler = MinMaxScaler(feature_range=(-1,1))
X = scaler.fit_transform(X)
encode = LabelEncoder()
y = encode.fit_transform(y)
#Train test splits
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=42)
xgb = XGBClassifier(learning_rate=0.05,n_estimators=500, objective="binary:logistic", scale_pos_weight=10, subsample=0.5) #Precision - 32%; Recall - 36%
xgb_model = xgb.fit(X_train, y_train)
#Predictions and evaluate
y_pred = xgb_model.predict(X_test)
print(classification_report(y_test, y_pred))
#Dump the model into the s3 bucket
with fs.open('s3://pdm-wbi/sam/Vestas_Gearbox/models/vestas_model.pickle','wb') as f:
            pickle.dump(xgb_model,f,protocol=3)
