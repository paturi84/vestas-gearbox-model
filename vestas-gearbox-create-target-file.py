import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn
from datetime import date, timedelta

master_table = pd.read_excel('s3://pdm-wbi/sam/Master_Table/Master Table.xlsx', header=0)
vestas = master_table.loc[master_table['Turbine Tech'].isin(['V47', 'V27', 'V80'])]
vestas['TURBINEDISPLAYNAME'] = vestas['Location'].str[:9]
vestas =  vestas[vestas['WO Asset with Backfill'].str.contains("Gearbox")]
vestas_severe = vestas[vestas['Total Cost'] >= 50000]
vestas_severe['Other.Work Order Work Group '] = vestas_severe['Other.Work Order Work Group '].fillna(0)
vestas_severe['Other.Work Order Work Group '] = vestas_severe['Other.Work Order Work Group '].astype('str')
vestas_severe = vestas_severe[vestas_severe['Other.Work Order Work Group '].str.contains("WCM-LOG")]
vestas_severe['Work Order Report Year '] = vestas_severe['Work Order Report Year '].astype('int')
vestas_severe['Work Order Report Date '] = pd.to_datetime(vestas_severe['Work Order Report Date '])
vestas_severe['Failure_Start_Date'] = pd.to_datetime(vestas_severe['Work Order Report Date '] - timedelta(180))
vestas_severe['Failure_End_Date'] = pd.to_datetime(vestas_severe['Work Order Actual Finish Date '])
vestas_severe.drop_duplicates(subset=['TURBINEDISPLAYNAME'], inplace=True)
vestas_severe = vestas_severe[['Location Plant', 'TURBINEDISPLAYNAME', 'Failure_Start_Date', 'Failure_End_Date' ]]
turbinedim = pd.read_csv('s3://gdms-workspace/sam/TURBINEDIM_30JAN19.csv', header=0)
vestas_severe = vestas_severe.merge(turbinedim, on=['TURBINEDISPLAYNAME'])
vestas_severe.to_csv('s3://pdm-wbi/sam/Vestas_Gearbox/vestas_target_file.csv')